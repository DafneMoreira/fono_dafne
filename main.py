from csv import reader


class Iris:
    def __init__(self):
        self.data_file = reader(open('iris.txt'))
        self.data = (linha for linha in self.data_file)
        print(self.data)


if __name__ == '__main__':
    Iris()
